/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file animalFarm.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <stdio.h>
#include <stdbool.h>
#include "catDatabase.h"
#include "addCat.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"
#include "config.h"
#define DEBUG

extern struct cat cats[];
extern int numberOfCats;

int main(){
   initialize();
   
   addCat( "Loki", MALE, PERSIAN, true, 8.5, BLACK, WHITE, 101 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0, BLACK, RED, 102 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2, BLACK, BLUE, 103 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2, BLACK, GREEN, 104 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2, BLACK, PINK, 105 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0, WHITE, BLACK, 106 ) ;


   printAllCats();
   int kali = findCat( "Kali" );
   updateCatName( kali, "Chili" ); // this should fail
   printCat( kali );
   updateCatName( kali, "Capulet" );
   updateCatWeight( kali, 9.9 );
   updateCatCollar1(kali, GREEN);
   updateCatCollar2(kali, BLACK);
   updateLicense(kali, 100);
   fixCat( kali );
   printCat( kali );

   #ifdef DEBUG   //this doesnt work... get this to work 
   char* exampleCollarCol;
   exampleCollarCol = collarColorName(cats[0].collarColor1);
   printf("The collar color is %s\n", exampleCollarCol);
   #endif

   printAllCats();
   deleteAllCats();
   printAllCats();

   return 0;
}
