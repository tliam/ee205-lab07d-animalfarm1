/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once
#include <stdbool.h>
#include "catDatabase.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"

void printCat(unsigned long);
void printAllCats();
int findCat(char catName[]);
char* collarColorName(const collarColor colorName);
