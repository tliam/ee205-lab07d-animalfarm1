/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "deleteCats.h"
extern struct cat cats[];
extern int numberOfCats;
void deleteAllCats(){
   for (int i = 0; i < MAX_CATS; i++){
      cats[i].isFixed = false;
      cats[i].weight = 0;
      memset(cats[i].name,0,strlen(cats[i].name));
   }
   //strcpy(name[i], "");
   //memset(name,0,strlen(name));
}
