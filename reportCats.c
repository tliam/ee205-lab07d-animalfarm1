/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#include "reportCats.h"
#define DEBUG

extern struct cat cats[];
extern int numberOfCats;

void printCat(unsigned long index){ 
   if (strlen(cats[index].name)==0 || index < 0){
      fprintf(stdout, "%s: Bad cat [%lu]\n", REPORT, index);
   }else{
      printf("cat index = [%lu] name = [%s], gender=[%d], breed=[%d], isFixed[%d], weight=[%f], collarColor1=[%d], collarColor2=[%d], license=[%llu]\n", index, cats[index].name, cats[index].genders, cats[index].breeds, cats[index].isFixed, cats[index].weight, cats[index].collarColor1, cats[index].collarColor2, cats[index].license);
   }
   return;
}

void printAllCats(){
   for(int i=0; i < MAX_CAT_NAME; i++){
      if (strlen(cats[i].name) != 0)
         printf("[%s] ", cats[i].name);
   }
   printf("\n");
   return;
}


int findCat(char catName[]){
   for ( int i = 0; i < MAX_CATS; i++ )
   {
      if (strcmp( cats[i].name, catName ) == 0){
         return i;
      }
   }
   exit(0);
}

char* collarColorName(const collarColor colorName){
   char collarCol[10]; //not quite okay, but good enough to test at the moment
   switch (colorName){
      case BLACK:
         strcpy(collarCol, "Black");
         break;
      case WHITE:
         strcpy(collarCol, "White");
         break;
      case RED:
         strcpy(collarCol, "Red");
         break;
      case BLUE:
         strcpy(collarCol, "Blue");
         break;
      case GREEN:
         strcpy(collarCol, "Green");
         break;
      case PINK:
         strcpy(collarCol, "Pink");
         break;
      default: //I forgot how to return a null for chars... is it /0?
         break;
   }
   #ifdef DEBUG
      printf("Color Of Collar: %s\n", collarCol);
   #endif
   return collarCol;  //I need to test the return of this var. I am getting warning from complier
}

