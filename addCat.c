/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "addCat.h"
//#define DEBUG
extern struct cat cats[];
extern int numberOfCats;

int addCat(char catName[], GenderType gender, BreedType breed, bool fixed, float catWeight,
      collarColor color1, collarColor color2, unsigned long long licenseNum){
#ifdef DEBUG
   printf("Name: %s, ", catName);
   printf("Gender: %s ", gender);
   printf("Breed: %d, ", breed);
   printf("Is fixed? %d, ", fixed);
   printf("CatWeight: %f, ", catWeight);
   printf("Collar Color: %d, ", color1);
   printf("Collar Color2: %d, ", color2);
   printf("License Number: %llu\n", licenseNum);
#endif


   if (catWeight <= 0 || strlen(catName)== 0 || strlen(catName) > MAX_CAT_NAME || numberOfCats > MAX_CATS){
      fprintf(stdout, "%s: Illegal Cat Info!\n", ADD);
      return 1; //1 = failed
   }
   for ( int i = 0; i < MAX_CATS; i++ )
   {
      if ( strcmp( cats[i].name, catName ) == 0 )
      {
         fprintf(stdout, "%s: Cat name %s is already in the database.\n", ADD, catName);
         return 1;
      }
   }

   cats[numberOfCats].isFixed = fixed;
   strcpy(cats[numberOfCats].name, catName);
   cats[numberOfCats].breeds = breed;
   cats[numberOfCats].genders = gender;
   cats[numberOfCats].weight = catWeight;
   cats[numberOfCats].collarColor1 = color1;
   cats[numberOfCats].collarColor2 = color2;
   cats[numberOfCats].license = licenseNum;
//everything here works and this basically tells how we can set and delete names for the rest of this lab.

#ifdef DEBUG
   for (int i = 0; i <= numberOfCats; i++){
      printf("Stored cat is fixed? %d\n", cats[i].isFixed);
      printf("Stored cat name: %s\n", cats[i].name);
   }
#endif
return numberOfCats++;

}

