/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#include "updateCats.h"

extern struct cat cats[];
extern int numberOfCats;

void updateCatName(unsigned long index, char newName[]){
   for (int i = 0; i < MAX_CAT_NAME; i++){
      if (strcmp( cats[i].name, newName ) == 0){
         fprintf(stdout, "%s: Name [%s] already in database\n", UPDATE, newName);
         return;
      }
   }
   if (index < 0 && index > MAX_CAT_NAME)
      return;

   strcpy(cats[index].name, newName);
   return;
}

void fixCat(unsigned long index){
   if (index < 0 && index > numberOfCats)
      return;

   cats[index].isFixed = true;
}

void updateCatWeight(unsigned long index, float newWeight){

   if (newWeight <= 0)
      return;

   cats[index].weight = newWeight;
}

void updateCatCollar1(unsigned long index, collarColor newCollarCol){
   cats[index].collarColor1 = newCollarCol;
}

void updateCatCollar2(unsigned long index, collarColor newCollarCol){
   cats[index].collarColor2 = newCollarCol;
}

void updateLicense(unsigned long index, unsigned long long newLicense){
   cats[index].license = newLicense;
}

