###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Liam Tapper <tliam@hawaii.edu>
### @date 13_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = animalFarm

all: $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCat.o: addCat.c addCat.h 
	$(CC) $(CFLAGS) -c addCat.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c catDatabase.h addCat.h updateCats.h deleteCats.h config.h
	$(CC) $(CFLAGS) -c main.c

main: main.o catDatabase.o addCat.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCat.o reportCats.o updateCats.o deleteCats.o


clean:
	rm -f $(TARGET) *.o
