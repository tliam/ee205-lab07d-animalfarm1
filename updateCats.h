/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once
#include "catDatabase.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "config.h"

void updateCatName(unsigned long index, char newName[]);
void fixCat(unsigned long index);
void updateCatWeight(unsigned long index, float newWeight);
void updateCatCollar1(unsigned long index, collarColor newCollarCol);
void updateCatCollar2(unsigned long index, collarColor newCollarCol);
void updateLicense(unsigned long index, unsigned long long newLicense);
