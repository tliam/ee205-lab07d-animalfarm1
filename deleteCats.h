/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
/// @file deleteCats.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 26_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once 
#include "catDatabase.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void deleteAllCats();
